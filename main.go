package main

import (
	"log"
	"net"
	"time"

	_ "net/http/pprof"

	"bitbucket.org/danielper/util/db"
	"bitbucket.org/danielper/util/msg"
	"github.com/buaazp/fasthttprouter"
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/reuseport"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func main() {
	counter := time.Now()
	log.SetPrefix("[INGESTOR] ")

	mongo, err := db.ConnectMongo("gookie")
	if err != nil {
		log.Fatal("Unable to connect to MongoDB")
	}

	closeKafka := msg.NewKafkaProducer()

	defer func() {
		mongo.Close()
		closeKafka()
	}()

	router, listener := setupHTTPReusePort()
	// router := setupHTTP()

	log.Println("Application running at :8080")
	log.Println(time.Since(counter))

	// log.Fatal(fasthttp.ListenAndServe(":8080", router.Handler))
	log.Fatal(fasthttp.Serve(listener, router.Handler))
}

func setupHTTP() *fasthttprouter.Router {
	router := fasthttprouter.New()

	MountRoutes(router)

	return router
}

func setupHTTPReusePort() (*fasthttprouter.Router, net.Listener) {
	router := fasthttprouter.New()

	MountRoutes(router)

	log.Fatal(fasthttp.ListenAndServe("0.0.0.0:8080", router.Handler))

	ln, err := reuseport.Listen("tcp4", "0.0.0.0:8080")
	if err != nil {
		log.Fatalf("error in reuseport listener: %s", err)
	}

	return router, ln
}
